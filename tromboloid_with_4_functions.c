//WAP to find the volume of a tromboloid using 4 functions.
#include <stdio.h>
int input() {
	int a;
    scanf("%d",&a);
    return a;
}

int volume(int h,int d,int b) {
    int v = (1/3)*((h*d*b)+(d/b));
    return v;
}

void print(int v) {
    printf("Volume of tromboloid is :%d",v);
}

void main() {
	printf("Input the value of h : \n");
    int h = input();    
    printf("Input the value of d : \n");
    int d = input();    
    printf("Input the value of b : \n");
    int b = input();
    int v = volume(h , d , b);
    print(v);  
}
