//WAP to find the sum of two fractions.
#include <stdio.h>

typedef struct {
    int num;
    int den;
}fract;

fract sum (fract f1, fract f2);

int main() {
    int n1, n2, d1, d2;
    printf("Enter the fraction 1(numerator denominator) : ");
    scanf("%d%d", &n1, &d1);
    printf("Enter the fraction 2(numerator denominator) : ");
    scanf("%d%d", &n2, &d2);
    fract f1 = {n1, d1};
    fract f2 = {n2, d2};
    fract result  = sum(f1,f2);
    printf("Result : %d / %d", result.num, result.den); 
}

fract sum(fract f1, fract f2) {
    fract result = {(f1.num * f2.den) + (f2.num * f1.den), f1.den * f2.den};
    return result;
}

