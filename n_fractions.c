//WAP to find the sum of n fractions.
#include <stdio.h>
typedef struct {
    int num;
    int deno;
}fract;

int getGCD(int a, int b) {
    if(b==0) return a;
    return getGCD(b, a%b);
}

int getLCM(int n, fract f[]) {
    int ans = f[0].deno;
    for(int i=1; i<n; i++) ans = (f[i].deno * ans) / getGCD(f[i].deno, ans);
    return ans;
}

fract sum(int n, fract f[]) {
    int fNum = 0;
    int fDeno = getLCM(n,f);
    for(int i=0; i<n; i++) 
        fNum = fNum + (f[i].num)*(fDeno/f[i].deno);
    fract ans = {fNum, fDeno};
    return ans;
}

int main() {
    int n, i;
    printf("Enter the number of fractions : \n");
    scanf("%d", &n);
    fract f[n];
    printf("Enter %d fractions (numerator denominator) : \n", n);
    for(int i=0; i<n; i++) scanf("%d%d", &f[i].num, &f[i].deno);
    fract result = sum(n,f);
    printf("Result : %d / %d", result.num, result.deno);
    return 0;
}	